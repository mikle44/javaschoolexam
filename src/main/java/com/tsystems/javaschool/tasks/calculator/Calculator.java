package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        } else {
            if (statement.length() != 0) {
                statement = preProcessing(statement);
            } else {
                return null;
            }
            if (statement != null) {
                statement = reversePolishNotation(statement);
            } else {
                return null;
            }
            if (statement != null) {
                statement = calculating(statement);
                return statement;
            } else {
                return null;
            }
        }
    }

    private static boolean isMathSymbol(char symbol) {
        switch (symbol) {
            case '+':
            case '-':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    private static byte priority(char priority) {
        switch (priority) {
            case '*':
            case '/':
                return 1;
        }
        return 0;
    }

    private static boolean isNumber(String element) {
        int count = 0;
        for (int i = 0; i < element.length(); i++) {
            if (!Character.isDigit(element.charAt(i))) {
                count++;
            }
        }
        return count == 0;
    }

    private static String preProcessing(String expression) {
        if (expression.charAt(0) == '-') {
            int decimal = 0;
            int i = 2;
            while (decimal == 0 && i < expression.length() - 1) {
                if (isMathSymbol(expression.charAt(i))) {
                    decimal = i;
                } else if (!isNumber(Character.toString(expression.charAt(i)))) {
                    return null;
                }
                i++;
            }
            expression = "(0" + expression.substring(0, decimal) + ")" + expression.substring(decimal);
        }
        for (int i = 0; i < expression.length() - 3; i++) {
            if (expression.charAt(i) == '(' && expression.charAt(i + 1) == '-') {
                expression = expression.substring(0, i + 1) + "0" + expression.substring(i + 1);
            }
        }
        return expression;
    }

    private static String reversePolishNotation(String expression) {
        StringBuilder operations = new StringBuilder();
        StringBuilder output = new StringBuilder();
        char inputChar;
        char tempChar;
        for (int i = 0; i < expression.length(); i++) {
            inputChar = expression.charAt(i);
            if (isMathSymbol(inputChar)) {
                while (operations.length() > 0) {
                    tempChar = operations.substring(operations.length() - 1).charAt(0);
                    if (isMathSymbol(tempChar) && (priority(inputChar) <= priority(tempChar))) {
                        output.append(" ");
                        output.append(tempChar);
                        operations.setLength(operations.length() - 1);
                    } else {
                        break;
                    }
                }
                output.append(" ");
                operations.append(inputChar);
            } else if (inputChar == '(') {
                operations.append(inputChar);
            } else if (inputChar == ')') {
                tempChar = operations.substring(operations.length() - 1).charAt(0);
                while (tempChar != '(') {
                    if (operations.length() == 1) {
                        return null;
                    } else {
                        output.append(" ");
                        output.append(tempChar);
                        operations.setLength(operations.length() - 1);
                        tempChar = operations.substring(operations.length() - 1).charAt(0);
                    }
                }
                operations.setLength(operations.length() - 1);
            } else {
                output.append(inputChar);
            }
        }
        while (operations.length() > 0) {
            output.append(" ");
            output.append(operations.substring(operations.length() - 1));
            operations.setLength(operations.length() - 1);
        }
        if (output.charAt(0) == " ".charAt(0)) {
            return null;
        } else {
            return output.toString();
        }
    }

    private static String calculating(String output) {
        Double operand1;
        Double operand2;
        Stack<Double> result = new Stack<>();
        String[] data = output.split(" ");
        for (String element : data) {
            if (element.isEmpty()) {
                return null;
            } else {
                if (isNumber(element)) {
                    result.push(Double.parseDouble(element));
                } else if (element.contains(".") && element.indexOf(".") == element.lastIndexOf(".")) {
                    result.push(Double.parseDouble(element));
                } else if (element.length() == 1 && isMathSymbol(element.charAt(0)) && result.size() >= 2) {
                    operand1 = result.pop();
                    operand2 = result.pop();
                    switch (element.charAt(0)) {
                        case '+':
                            result.push(operand1 + operand2);
                            break;
                        case '-':
                            result.push(operand2 - operand1);
                            break;
                        case '*':
                            result.push(operand1 * operand2);
                            break;
                        case '/':
                            if (operand1 != 0) {
                                result.push(operand2 / operand1);
                            } else {
                                return null;
                            }
                            break;
                    }
                } else {
                    return null;
                }
            }
        }
        DecimalFormat df = new DecimalFormat("#.####", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setRoundingMode(RoundingMode.HALF_UP);
        return df.format(result.pop());
    }

}
