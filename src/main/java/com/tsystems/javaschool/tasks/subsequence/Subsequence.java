package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException("Wrong input");
        if (x.size() == 0) {
            return true;
        }
        if (x.size() > y.size()) {
            return false;
        }
        ArrayList<Object> subString = new ArrayList<Object>(x);
        ArrayList<Object> inputString = new ArrayList<Object>(y);
        List<List<Integer>> indexes = new ArrayList<>();
        for (int i = 0; i < subString.size(); i++) {
            indexes.add(new ArrayList<>());
            for (int j = 0; j < inputString.size(); j++) {
                if (subString.get(i).equals(inputString.get(j))) {
                    indexes.get(i).add(j);
                }
            }
        }
        return check(indexes);
    }

    private static boolean check(List<List<Integer>> indexes) {
        int c = indexes.get(0).get(0);
        for (int i = 1; i < indexes.size(); i++) {
            for (int j = 0; j < indexes.get(i).size(); j++) {
                if (indexes.get(i).get(j) <= c && j == indexes.get(i).size() - 1) {
                    return false;
                } else if (indexes.get(i).get(j) > c) {
                    c = indexes.get(i).get(j);
                }
            }
        }
        return true;
    }
}
