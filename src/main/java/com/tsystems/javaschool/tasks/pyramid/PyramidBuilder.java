package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        for (Integer inputNumber : inputNumbers) {
            if (inputNumber == null) throw new CannotBuildPyramidException();
        }
        if (inputNumbers.get(0) == 0) {
            for (Integer element : inputNumbers) {
                if (element.equals(inputNumbers.get(0))) {
                }
                throw new CannotBuildPyramidException();
            }
        }
        int count = 0;
        int countLines = 0;
        int lineLength = 0;
        for (int i = 0; count < inputNumbers.size(); i++) {
            countLines = i;
            count = countLines * (countLines + 1) / 2;
            lineLength = countLines * 2 - 1;
        }
        if (count != inputNumbers.size()) throw new CannotBuildPyramidException();
        return builder(inputNumbers, countLines, lineLength);
    }

    private static List<Integer> insertSort(List<Integer> inputNumbers) {
        for (int i = 1; i < inputNumbers.size(); i++) {
            int temp = inputNumbers.get(i);
            for (int j = i - 1; j >= 0 && inputNumbers.get(j) > temp; j--) {
                inputNumbers.set(j + 1, inputNumbers.get(j));
                inputNumbers.set(j, temp);
            }
        }
        return inputNumbers;
    }

    private static int[][] builder(List<Integer> inputNumbers, int countLines, int lineLength) {
        List<Integer> sortedInputNumbers = insertSort(inputNumbers);
        int[][] output;
        output = new int[countLines][lineLength];
        int center = lineLength / 2;
        int index = 0;
        for (int i = 0; i < countLines; i++) {
            if (i % 2 == 0) {
                for (int j = i - 1; j > 0; j = j - 2, index++) {
                    output[i][center - 1 - j] = sortedInputNumbers.get(index);
                }
                output[i][center] = sortedInputNumbers.get(index);
                index++;
                for (int j = 1; j <= i - 1; j = j + 2, index++) {
                    output[i][center + 1 + j] = sortedInputNumbers.get(index);
                }
            } else {
                for (int j = i - 1; j >= 0; j = j - 2, index++) {
                    output[i][center - 1 - j] = sortedInputNumbers.get(index);
                }
                for (int j = 0; j <= i - 1; j = j + 2, index++) {
                    output[i][center + 1 + j] = sortedInputNumbers.get(index);
                }
            }
        }
        return output;
    }

}
